package com.bidv.common.utils.device

import android.content.Context
import android.util.DisplayMetrics
import android.util.TypedValue
import kotlin.math.roundToInt

object DimentionUtil {
    fun dpToPx(dp: Float, ctx: Context): Float {
        val displayMetrics = ctx.resources.displayMetrics
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, displayMetrics)
    }

    fun pxToDp(px: Float, ctx: Context): Float {
        val displayMetrics = ctx.resources.displayMetrics
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, px, displayMetrics)
    }
}