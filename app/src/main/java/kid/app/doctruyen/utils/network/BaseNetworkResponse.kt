package kid.app.doctruyen.utils.network

data class BaseNetworkResponse<T>(
        val data: T?,
        val message: String?,
        val status: String
) {
    val isSuccess: Boolean get() = status == "0"

    companion object {
        fun <T> success(data: T): BaseNetworkResponse<T> =
                BaseNetworkResponse(status = "0", data = data, message = null)

        fun <T> error(message: String): BaseNetworkResponse<T> =
                BaseNetworkResponse(status = "999999", data = null, message =  null)

        fun <T> successWithNoData(): BaseNetworkResponse<T> =
                BaseNetworkResponse(status = "0", data = null, message = null)

    }
}