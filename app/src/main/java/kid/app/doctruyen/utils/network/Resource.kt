package kid.app.doctruyen.utils.network

data class Resource<out T>(val status: Status, val data: T?, var message: String?) {

    val isSuccess get() =  status == Status.SUCCESS
    val isLoading get() = status == Status.LOADING

    companion object {
        fun <T> success(data: T,message: String? = ""): Resource<T> =
                Resource(status = Status.SUCCESS, data = data, message = message)

        fun <T> error(message: String, data: T? = null): Resource<T> =
                Resource(status = Status.ERROR, data = data, message = message)

        fun <T> loading(data: T? = null): Resource<T> =
                Resource(status = Status.LOADING, data = data, message = null)
    }
}