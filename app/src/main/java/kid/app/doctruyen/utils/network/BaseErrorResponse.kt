package kid.app.doctruyen.utils.network

data class BaseErrorResponse(
    val code: Int,
    val message: String?
)
