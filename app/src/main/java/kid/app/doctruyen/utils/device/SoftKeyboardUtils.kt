package com.bidv.common.utils.device

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.view.View
import android.view.inputmethod.InputMethodManager


object SoftKeyboardUtils {
    fun hideSoftKeyboard(context: Context) {
        val act = getActivityFromContext(context)
        if (act == null || act.isFinishing) {
            return
        }
        val imm = act.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm?.hideSoftInputFromWindow(act.window.decorView.windowToken, 0)

//        if (imm.isActive) imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS)
    }

    fun hideSoftKeyboard(view: View) {
        val imm = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showSoftKeyboard(context: Context, view: View?) {
        val act = getActivityFromContext(context)
        if (act == null || act.isFinishing
            || view == null
        ) {
            return
        }
        val imm = act.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (imm != null) {
            view.requestFocus()
            imm.showSoftInput(view, 0)
        }
    }

    private fun getActivityFromContext(context: Context): Activity? {
        var context: Context? = context
        var act: Activity? = null
        if (context is Activity) {
            act = context
        } else {
            while (context is ContextWrapper) {
                if (context is Activity) {
                    act = context
                    break
                }
                context = context.baseContext
            }
        }
        return act
    }
}