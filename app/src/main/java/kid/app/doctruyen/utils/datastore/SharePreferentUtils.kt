package com.bidv.common.utils.datastore

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.GsonBuilder

object SharePreferencesUtils {

    lateinit var sharedPreferences: SharedPreferences

    private fun initSharedPreferences(context: Context) {
        if (::sharedPreferences.isInitialized) {
            return
        }
        sharedPreferences =
            context.getSharedPreferences(context.applicationInfo.packageName, Context.MODE_PRIVATE)
    }

    fun saveString(key: String, value: String, context: Context) {
        initSharedPreferences(context)
        with(sharedPreferences.edit()) {
            putString(key, value)
            apply()
        }
    }

    fun saveInt(key: String, value: Int, context: Context) {
        initSharedPreferences(context)
        with(sharedPreferences.edit()) {
            putInt(key, value)
            apply()
        }
    }

    fun getInt(key: String, context: Context): Int {
        initSharedPreferences(context)
        return sharedPreferences.getInt(key, 0)!!
    }

    fun saveBoolean(key: String, value: Boolean, context: Context) {
        initSharedPreferences(context)
        with(sharedPreferences.edit()) {
            putBoolean(key, value)
            apply()
        }
    }

    fun getBoolean(key: String, context: Context): Boolean {
        initSharedPreferences(context)
        return sharedPreferences.getBoolean(key, false)
    }

    fun getString(key: String, context: Context): String {
        initSharedPreferences(context)
        return sharedPreferences.getString(key, "")!!
    }

    fun with(context: Context) {
        if (::sharedPreferences.isInitialized) {
            return
        }
        sharedPreferences =
            context.getSharedPreferences(context.applicationInfo.packageName, Context.MODE_PRIVATE)
    }

    fun <T> putObject(obj: T, key: String){
        val jsonString = GsonBuilder().create().toJson(obj)
        sharedPreferences.edit().putString(key, jsonString).apply()
    }

    inline fun <reified T> getObject(key: String): T? {
        val value = sharedPreferences.getString(key, null)
        return GsonBuilder().create().fromJson(value, T::class.java)
    }
}