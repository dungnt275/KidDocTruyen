package kid.app.doctruyen.utils.network

import com.skydoves.sandwich.ApiResponse
import kid.app.doctruyen.data.api.ApiService
import kid.app.doctruyen.data.api.request.StoryRequest
import kid.app.doctruyen.data.model.Category
import kid.app.doctruyen.data.model.Genre
import kid.app.doctruyen.data.model.Story
import javax.inject.Inject

class ApiClient @Inject constructor(
  private val apiService: ApiService
) {

  suspend fun fetchCategoryList(): ApiResponse<BaseNetworkResponse<BaseListObjectResponse<Category>>> =
    apiService.getCategories()

  suspend fun getGenres(): ApiResponse<BaseNetworkResponse<BaseListObjectResponse<Genre>>> =
    apiService.getGenres()

  suspend fun fetchStoryByCategory(category: Category, page: Int, limit: Int): BaseNetworkResponse<BaseListObjectResponse<Story>> =
    apiService.getStoryByCategory(category.link, StoryRequest(genres = null, page = page, limit = limit))

  companion object {
    private const val PAGING_SIZE = 20
  }
}
