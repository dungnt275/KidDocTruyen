package kid.app.doctruyen.utils.network

import com.google.gson.annotations.SerializedName

data class BaseListObjectResponse<T>(
//        @SerializedName("total")
//        val total: Int = 10,
//        @SerializedName("total_pages")
//        val total_pages: Int = 0,
//        @SerializedName("page")
//        val page: Int = 0,
//        @SerializedName("limit")
//        val limit: Int = 0,
        @SerializedName("items")
        val items: List<T>
)