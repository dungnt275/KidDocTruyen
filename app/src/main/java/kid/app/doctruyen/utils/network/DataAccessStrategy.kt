package kid.app.doctruyen.utils.network

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.map
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

fun <T, A> performGetOperation(
    databaseQuery: () -> LiveData<T>,
    networkCall: suspend () -> Resource<A>,
    saveCallResult: suspend (A) -> Unit,
): LiveData<Resource<T>> =
    liveData(Dispatchers.IO) {
        emit(Resource.loading())
        val source = databaseQuery.invoke().map { Resource.success(it) }
        emitSource(source)
        val response = networkCall.invoke()
        if (response.status == Status.SUCCESS) {
            saveCallResult(response.data!!)
        } else if (response.status == Status.ERROR) {
            emit(Resource.error(response.message!!))
        }
    }

fun <T> performGetRequest(
    networkCall: suspend () -> Resource<T>,
): LiveData<Resource<T>> =
    liveData(Dispatchers.IO) {
        emit(Resource.loading())
        val response = networkCall.invoke()
        emit(response)
    }

fun <T> performGetRequestWithCache(
    databaseQuery: () -> LiveData<BaseNetworkResponse<T>>,
    networkCall: suspend () -> Resource<BaseNetworkResponse<T>>,
    saveCallResult: suspend (T) -> Unit,
): LiveData<Resource<BaseNetworkResponse<T>>> =
    liveData(Dispatchers.IO) {
        emit(Resource.loading())
        val source = databaseQuery.invoke().map { Resource.success(it) }
        emitSource(source)
        val response = networkCall.invoke()
        if (response.isSuccess && response.data?.isSuccess == true
            && response?.data?.data != null) {
            saveCallResult(response.data?.data!!)
        } else if (response.status == Status.ERROR) {
            emit(Resource.error(response.message ?: response.data?.message ?: ""))
        }
    }

fun <T> performGetLocalOperation(
    databaseQuery: () -> LiveData<T>,
): LiveData<Resource<T>> =
    liveData(Dispatchers.IO) {
        emit(Resource.loading())
        val source = databaseQuery.invoke().map { Resource.success(it) }
        emitSource(source)
    }

fun performLocalDatabaseAction(
    databaseQuery: () -> Unit,
) = GlobalScope.launch {
    databaseQuery.invoke()
}

fun <A> performUpdateFromRemote(
    networkCall: suspend () -> Resource<A>,
    saveCallResult: suspend (A) -> Unit,
) = GlobalScope.launch {
    val response = networkCall.invoke()
    if (response.status == Status.SUCCESS) {
        saveCallResult(response.data!!)
    }
}


fun <T> performRemoteOperation(
    networkCall: suspend () -> Resource<T>,
): LiveData<Resource<T>> =
    liveData(Dispatchers.IO) {
        emit(Resource.loading())
        val response = networkCall.invoke()
        emit(response)
    }