package kid.app.doctruyen.utils.network

data class Result<T>(val code: Int, val message: String, val data: T)