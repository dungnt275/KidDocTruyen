package kid.app.doctruyen.utils.network

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.withContext

abstract class NetworkBoundResourceFlow<ResultType>(private val enableCache: Boolean) {

    suspend fun asFlow(): Flow<Resource<ResultType>> {
        return withContext(Dispatchers.IO) {
            flow {
                emit(Resource.loading(null))
                if (enableCache){
                    val dbSource = loadFromDb()//.first()
                    if (shouldFetch(dbSource)) {
                        fetchFromNetwork(this, dbSource)
                    } else {
                        emit(Resource.success(dbSource))
                    }
                } else {
                    fetchFromNetwork(this, null)
                }
            }
        }
    }

    protected open fun onFetchFailed(){
        // Implement in sub-classes to handle errors
    }

    protected open fun processResponse(response: ApiSuccessResponse<Result<ResultType>>) = response.body

    protected abstract suspend fun saveNetworkResult(item: ResultType)

    private suspend fun fetchFromNetwork(flowCollector: FlowCollector<Resource<ResultType>>, dbSource: ResultType?) {
        when (val apiResponse = createCall()){
            is ApiSuccessResponse -> {
                val result = processResponse(apiResponse)
                if (enableCache) {
                    saveNetworkResult(result.data)
                    val newDbSource = loadFromDb()
                    flowCollector.emit(Resource.success( newDbSource ?: result.data, result.message))
                } else {
                    if (result.code == 0){
                        flowCollector.emit(Resource.success(result.data, result.message))
                    } else {
                        flowCollector.emit(Resource.error(result.message, result.data))
                    }
                }
            }
            is ApiEmptyResponse -> {
                flowCollector.emit(Resource.error("ApiEmptyResponse", dbSource))
            }
            is ApiErrorResponse -> {
                onFetchFailed()
                flowCollector.emit(Resource.error(apiResponse.errorMessage, dbSource))
            }
        }
    }

    protected abstract fun shouldFetch(data: ResultType?): Boolean

    protected abstract fun loadFromDb(): ResultType

    protected abstract suspend fun createCall(): ApiResponse<Result<ResultType>>
}