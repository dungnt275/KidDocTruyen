package kid.app.doctruyen.utils.constants

object AppConstant {
    // api version
    const val API_VERSION = "v1"

    // Database
    const val DB_NAME = "kiddoctruyen.db"

    // Api
    const val NETWORK_PAGE_SIZE = 3//10
    const val STORY_START_PAGE_INDEX = 1

    object DATE_TIME {
        const val DATE_FORMAT1 = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        const val DATE_FORMAT2 = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        const val MINUTE_TO_SECONDS = 60
        const val HOUR_TO_MINUTES = 60
        const val DAY_TO_HOURS = 24
        const val WEEK_TO_DAYS = 7
        const val SUFFIX = "trước"
        const val WEEK = "tuần"
        const val DAY = "ngày"
        const val HOUR = "giờ"
        const val MINUTE = "phút"
        const val SECOND = "giây"
    }

    object ARGS{
        const val KEY_CATEGORY = "story_category"
    }
}