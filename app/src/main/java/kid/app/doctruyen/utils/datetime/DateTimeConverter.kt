package kid.app.doctruyen.utils.datetime

import kid.app.doctruyen.utils.constants.AppConstant
import kid.app.doctruyen.utils.constants.AppConstant.DATE_TIME.DAY
import kid.app.doctruyen.utils.constants.AppConstant.DATE_TIME.DAY_TO_HOURS
import kid.app.doctruyen.utils.constants.AppConstant.DATE_TIME.HOUR
import kid.app.doctruyen.utils.constants.AppConstant.DATE_TIME.HOUR_TO_MINUTES
import kid.app.doctruyen.utils.constants.AppConstant.DATE_TIME.MINUTE
import kid.app.doctruyen.utils.constants.AppConstant.DATE_TIME.MINUTE_TO_SECONDS
import kid.app.doctruyen.utils.constants.AppConstant.DATE_TIME.SECOND
import kid.app.doctruyen.utils.constants.AppConstant.DATE_TIME.SUFFIX
import kid.app.doctruyen.utils.constants.AppConstant.DATE_TIME.WEEK
import kid.app.doctruyen.utils.constants.AppConstant.DATE_TIME.WEEK_TO_DAYS
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DateTimeConverter {
    fun fromDateToMilliseconds(dateString: String, format: String): Long {
        return try {
            val dateFormat = SimpleDateFormat(format)
            dateFormat.timeZone = TimeZone.getTimeZone("UTC")
            val date: Date = dateFormat.parse(dateString)
            return date.time
        } catch (e: ParseException){
            -1
        }
    }

    fun convertToReadableDuration(timeInMilliseconds: Long): String {
        var time = timeInMilliseconds / 1000
        val weeks = time / (WEEK_TO_DAYS* DAY_TO_HOURS* HOUR_TO_MINUTES* MINUTE_TO_SECONDS)
        time %= (WEEK_TO_DAYS* DAY_TO_HOURS* HOUR_TO_MINUTES* MINUTE_TO_SECONDS)
        val days = time / (DAY_TO_HOURS* HOUR_TO_MINUTES* MINUTE_TO_SECONDS)
        time %= (DAY_TO_HOURS* HOUR_TO_MINUTES* MINUTE_TO_SECONDS)
        val hours = time / (HOUR_TO_MINUTES* MINUTE_TO_SECONDS)
        time %= (HOUR_TO_MINUTES* MINUTE_TO_SECONDS)
        val minutes = time / (MINUTE_TO_SECONDS)
        time %= (MINUTE_TO_SECONDS)
        val seconds = time

        if (weeks > 0) return "$weeks $WEEK $SUFFIX"
        if (days > 0) return "$days $DAY $SUFFIX"
        if (hours > 0 ) return "$hours $HOUR $SUFFIX"
        if (minutes > 0) return "$minutes $MINUTE $SUFFIX"
        return "$seconds $SECOND $SUFFIX"
    }
}