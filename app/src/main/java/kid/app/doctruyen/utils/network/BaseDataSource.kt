package kid.app.doctruyen.utils.network

import kid.app.doctruyen.R
import kid.app.doctruyen.app.App
import retrofit2.Response

abstract class BaseDataSource {
    protected val splitErrorCode = "---"

    protected open suspend fun <T> getResult(call: suspend () -> Response<T>): Resource<T> {
        try {
            val response = call()
            if (response.isSuccessful) {
                val body = response.body()
                if (body != null){
                    if (body is BaseNetworkResponse<*>) {
                        return if (body.isSuccess) {
                            Resource.success(body)
                        } else {
                            Resource.error(body.message ?: App.instance.getString(R.string.have_error_when_load_data), body)
                        }
                    }
                    return Resource.success(body)
                }
            }

            return error("${response.code()}$splitErrorCode${App.instance.getString(R.string.have_error_when_load_data)}")
        } catch (e: Exception) {
            return error(if(e is NoConnectivityException) e.message else App.instance.getString(R.string.have_error_when_load_data))
        }
    }

    private fun <T> error(message: String): Resource<T> {
        return Resource.error(message)
    }


}