package com.bidv.common.utils.device

import android.R
import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Build
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import java.util.*

object DeviceUtil {
    fun getScreenWidth(ctx: Activity): Int {
        val dm = DisplayMetrics()
        ctx.windowManager.defaultDisplay.getMetrics(dm)
        return dm.widthPixels
    }

    fun getScreenHeight(ctx: Activity): Int {
        val dm = DisplayMetrics()
        ctx.windowManager.defaultDisplay.getMetrics(dm)
        return dm.heightPixels
    }

    fun getActionbarHeight(act: Context): Int {
        val tv = TypedValue()
        return if (act.theme.resolveAttribute(R.attr.actionBarSize, tv, true)) {
            TypedValue.complexToDimensionPixelSize(tv.data, act.resources.displayMetrics)
        } else 0
    }

    fun makeFullScreen(fullScreen: Boolean, act: AppCompatActivity) {
        try {
            if (fullScreen) {
                // if (Build.VERSION.SDK_INT < 16) {
                act.window.setFlags(
                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN
                )
                // } else {
                // View decorView = act.getWindow().getDecorView();
                // // Hide the status bar.
                // decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN
                // );
                // }
                act.supportActionBar!!.hide()
            } else {
                // if (Build.VERSION.SDK_INT < 16) {
                act.window.clearFlags(
                    WindowManager.LayoutParams.FLAG_FULLSCREEN
                )
                // } else {
                // View decorView = act.getWindow().getDecorView();
                // // Hide the status bar.
                // int uiOptions = View.SYSTEM_UI_FLAG_VISIBLE;
                // decorView.setSystemUiVisibility(uiOptions);
                // }
                act.supportActionBar!!.show()
            }
        } catch (e: Exception) {
            // TODO: handle exception
        }
    }

    fun getVersionName(ctx: Context): String? {
        val pInfo: PackageInfo
        return try {
            pInfo = ctx.packageManager.getPackageInfo(ctx.packageName, 0)
            pInfo.versionName
        } catch (e: PackageManager.NameNotFoundException) {
            null
        }
    }

    fun isHaveSoftNavigationBar(context: Context): Boolean {
        //Emulator
        if (Build.FINGERPRINT.startsWith("generic")) {
            return true
        }
        val id = context.resources.getIdentifier("config_showNavigationBar", "bool", "android")
        return id > 0 && context.resources.getBoolean(id)
    }

    fun copyPinToClipboad(ctx: Context, value: String?, label: String?) {
        val clipboard = ctx.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText(label, value)
        clipboard.setPrimaryClip(clip)
    }

    /**
     * Hide Navigation bar control
     * @param activity
     */
    fun hideDefaultControls(activity: Activity) {
        val window = activity.window ?: return
        window.clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN)
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        val decorView = window.decorView
        if (decorView != null) {
            var uiOptions = decorView.systemUiVisibility
            if (Build.VERSION.SDK_INT >= 14) {
                uiOptions = uiOptions or View.SYSTEM_UI_FLAG_LOW_PROFILE
            }
            if (Build.VERSION.SDK_INT >= 16) {
                uiOptions = uiOptions or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            }
            if (Build.VERSION.SDK_INT >= 19) {
                uiOptions = uiOptions or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            }
            decorView.systemUiVisibility = uiOptions
        }
    }

    /**
     * Show Navigation bar control
     * @param activity
     */
    fun showDefaultControls(activity: Activity) {
        val window = activity.window ?: return
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        window.addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN)
        val decorView = window.decorView
        if (decorView != null) {
            var uiOptions = decorView.systemUiVisibility
            if (Build.VERSION.SDK_INT >= 14) {
                uiOptions = uiOptions and View.SYSTEM_UI_FLAG_LOW_PROFILE.inv()
            }
            if (Build.VERSION.SDK_INT >= 16) {
                uiOptions = uiOptions and View.SYSTEM_UI_FLAG_HIDE_NAVIGATION.inv()
            }
            if (Build.VERSION.SDK_INT >= 19) {
                uiOptions = uiOptions and View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY.inv()
            }
            decorView.systemUiVisibility = uiOptions
        }
    }

}