package kid.app.doctruyen.utils.network

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}