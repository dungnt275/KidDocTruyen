package kid.app.doctruyen.utils.network

import kid.app.doctruyen.R
import kid.app.doctruyen.app.App
import java.io.IOException

class NoConnectivityException : IOException() {
    override val message: String
        get() = App.instance.getString(R.string.no_connection_error)
}