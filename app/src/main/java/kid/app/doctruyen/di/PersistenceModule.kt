package kid.app.doctruyen.di

import android.app.Application
import androidx.room.Room
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kid.app.doctruyen.data.database.AppDatabase
import kid.app.doctruyen.data.database.CategoryDao
import kid.app.doctruyen.utils.constants.AppConstant
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object PersistenceModule {

    @Provides
    @Singleton
    fun provideGson(): Gson {
        return Gson()
    }

    @Provides
    @Singleton
    fun provideAppDatabase(
        application: Application,
//        typeResponseConverter: TypeResponseConverter
    ): AppDatabase {
        return Room
            .databaseBuilder(application, AppDatabase::class.java, AppConstant.DB_NAME)
            .fallbackToDestructiveMigration()
//            .addTypeConverter(typeResponseConverter)
            .build()
    }

    @Singleton
    @Provides
    fun provideCategoryDao(db: AppDatabase) = db.categoryDao()

    @Singleton
    @Provides
    fun provideGenreDao(db: AppDatabase) = db.genreDao()

//    @Provides
//    @Singleton
//    fun provideTypeResponseConverter(moshi: Moshi): TypeResponseConverter {
//        return TypeResponseConverter(moshi)
//    }
}