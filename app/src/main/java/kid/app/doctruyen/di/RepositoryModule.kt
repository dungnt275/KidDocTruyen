package kid.app.doctruyen.di

import android.content.Context
import com.google.gson.GsonBuilder
import com.skydoves.sandwich.coroutines.CoroutinesResponseCallAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ViewModelScoped
import dagger.hilt.components.SingletonComponent
import kid.app.doctruyen.BuildConfig
import kid.app.doctruyen.R
import kid.app.doctruyen.app.App
import kid.app.doctruyen.data.database.AppDatabase
import kid.app.doctruyen.data.database.CategoryDao
import kid.app.doctruyen.data.database.GenreDao
import kid.app.doctruyen.data.api.ApiService
import kid.app.doctruyen.data.datasource.AppDataSource
import kid.app.doctruyen.data.datasource.StoryDataSource
import kid.app.doctruyen.data.repository.AppRepository
import kid.app.doctruyen.utils.network.ApiClient
import kid.app.doctruyen.utils.network.NetworkConstant
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(ViewModelComponent::class)
object RepositoryModule {

    @Provides
    @ViewModelScoped
    fun provideAppRepository(remoteDataSource: AppDataSource,
                             apiClient: ApiClient,
                          categoryDao: CategoryDao,
                          genreDao: GenreDao) =
        AppRepository(remoteDataSource, apiClient, categoryDao, genreDao)


}