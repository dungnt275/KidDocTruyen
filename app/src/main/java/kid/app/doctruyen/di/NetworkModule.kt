/*
 * Designed and developed by 2020 skydoves (Jaewoong Eum)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kid.app.doctruyen.di

import com.google.gson.GsonBuilder
import com.skydoves.sandwich.coroutines.CoroutinesResponseCallAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kid.app.doctruyen.BuildConfig
import kid.app.doctruyen.R
import kid.app.doctruyen.app.App
import kid.app.doctruyen.data.api.ApiService
import kid.app.doctruyen.utils.network.ApiClient
import kid.app.doctruyen.utils.network.NetworkConstant
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

  @Singleton
  @Provides
  fun provideOkHttpClient(): OkHttpClient {
    val loggingInterceptor = HttpLoggingInterceptor().apply {
      level = when (BuildConfig.DEBUG) {
        true -> HttpLoggingInterceptor.Level.BODY
        else -> HttpLoggingInterceptor.Level.NONE
      }
    }
    val okHttpClient = OkHttpClient.Builder().apply {
      connectTimeout(NetworkConstant.TIME_OUT, TimeUnit.SECONDS)
      readTimeout(NetworkConstant.TIME_OUT, TimeUnit.SECONDS)
      writeTimeout(NetworkConstant.TIME_OUT, TimeUnit.SECONDS)
//            retryOnConnectionFailure(false)
//            addInterceptor(NetWorkHeaderInterceptor())
//            addInterceptor(SecurityInterceptor())
      addInterceptor(loggingInterceptor)

    }.build()
    okHttpClient.dispatcher.maxRequestsPerHost = 7
    return okHttpClient
  }

  @Singleton
  @Provides
  fun provideRetrofit(okHttpClient: OkHttpClient) : Retrofit {
//        val headerInterceptor = HttpLoggingInterceptor().apply {
//            level = HttpLoggingInterceptor.Level.HEADERS
//        }
//        val loggingInterceptor = HttpLoggingInterceptor().apply {
//            level = when (BuildConfig.DEBUG) {
//                true -> HttpLoggingInterceptor.Level.BODY
//                else -> HttpLoggingInterceptor.Level.NONE
//            }
//        }
//
//        val okHttpClient =  OkHttpClient.Builder().apply {
//            connectTimeout(NetworkConstantIBank.TIME_OUT, TimeUnit.SECONDS)
//            readTimeout(NetworkConstantIBank.TIME_OUT, TimeUnit.SECONDS)
//            writeTimeout(NetworkConstantIBank.TIME_OUT, TimeUnit.SECONDS)
//            retryOnConnectionFailure(false)
////            addInterceptor(headerInterceptor)
////            addInterceptor(HeaderInterceptor(ApplicationBase.instance.applicationContext))
////            addInterceptor(MockInterceptor())
//            addInterceptor(NetworkInterceptor())
//            addInterceptor(loggingInterceptor)
//
//        }.build()
    return Retrofit.Builder()
      .baseUrl(App.instance.getString(R.string.base_url))
      .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setLenient().create()))
      .addCallAdapterFactory(CoroutinesResponseCallAdapterFactory())
      .client(okHttpClient)
      .build()
  }

  @Singleton
  @Provides
  fun provideApiService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)

  @Provides
  @Singleton
  fun provideApiClient(apiService: ApiService): ApiClient {
    return ApiClient(apiService)
  }
}
