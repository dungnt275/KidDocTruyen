package kid.app.doctruyen.app

import android.app.Application
import com.bidv.common.utils.datastore.SharePreferencesUtils
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

@HiltAndroidApp
class App : Application() {
    init {
        instance = this
    }

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        SharePreferencesUtils.with(this)
    }

    companion object {
        lateinit var instance: Application
            private set
    }
}