package kid.app.doctruyen.data.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "genres")
@Parcelize
data class Genre(
    @SerializedName("genreId")
    @PrimaryKey
    val genreId: Int,
    @SerializedName("name")
    val name: String,
//    @SerializedName("__v")
//    val __v: Int
    val total: Int = 100
): Parcelable
