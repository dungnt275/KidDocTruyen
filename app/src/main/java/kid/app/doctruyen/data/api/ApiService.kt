package kid.app.doctruyen.data.api

import com.skydoves.sandwich.ApiResponse
import kid.app.doctruyen.data.api.request.StoryByChapterRequest
import kid.app.doctruyen.data.model.Category
import kid.app.doctruyen.data.model.Genre
import kid.app.doctruyen.data.model.Story
import kid.app.doctruyen.data.api.request.StoryRequest
import kid.app.doctruyen.data.api.response.StoryByChapterResponse
import kid.app.doctruyen.utils.constants.AppConstant.API_VERSION
import kid.app.doctruyen.utils.network.BaseListObjectResponse
import kid.app.doctruyen.utils.network.BaseNetworkResponse
import retrofit2.Response
import retrofit2.http.*

interface ApiService {
    // lấy categories
    @POST("$API_VERSION/categories")
//    suspend fun getCategories(): Response<BaseNetworkResponse<BaseListObjectResponse<Category>>>
    suspend fun getCategories(): ApiResponse<BaseNetworkResponse<BaseListObjectResponse<Category>>>

    //lấy thể loại truyện
    @POST("$API_VERSION/genres")
    suspend fun getGenres(): ApiResponse<BaseNetworkResponse<BaseListObjectResponse<Genre>>>

    @POST("{categoryLink}")
    suspend fun getStoryByCategory(@Path("categoryLink", encoded = true) link: String, @Body body: StoryRequest): BaseNetworkResponse<BaseListObjectResponse<Story>>

    @POST("$API_VERSION/chapters")
    suspend fun getStoryByChapter(@Body body: StoryByChapterRequest): Response<BaseNetworkResponse<StoryByChapterResponse>>
}