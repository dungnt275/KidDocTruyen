package kid.app.doctruyen.data.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kid.app.doctruyen.data.model.Genre

@Dao
interface GenreDao {
    @Query("SELECT * FROM genres")
    fun getAllGenres(): List<Genre>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(genres: List<Genre>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(genre: Genre)

    @Query("SELECT name FROM genres WHERE genreId = :id")
    suspend fun getGenreNameById(id: Int): String
}