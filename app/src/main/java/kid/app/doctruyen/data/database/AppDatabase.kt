package kid.app.doctruyen.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import kid.app.doctruyen.data.model.Category
import kid.app.doctruyen.data.model.Genre
import kid.app.doctruyen.utils.constants.AppConstant

@Database(entities = [Category::class, Genre::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun categoryDao(): CategoryDao
    abstract fun genreDao(): GenreDao

    companion object {
        @Volatile private var instance: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase =
            instance ?: synchronized(this){ instance ?: buildDatabase(context).also { instance = it }}

        private fun buildDatabase(appContext: Context) =
            Room.databaseBuilder(appContext, AppDatabase::class.java, AppConstant.DB_NAME)
                .fallbackToDestructiveMigration()
                .build()
    }
}