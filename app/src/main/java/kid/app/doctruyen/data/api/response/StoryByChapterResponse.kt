package kid.app.doctruyen.data.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class StoryByChapterResponse(
        @SerializedName("nextChapId")
        val nextChapId: String,
        @SerializedName("prevChapId")
        val prevChapId: String,
        @SerializedName("data")
        val data: String,
        @SerializedName("_id")
        val _id: String,
        @SerializedName("title")
        val title: String
): Parcelable
