package kid.app.doctruyen.data.datasource

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.skydoves.sandwich.suspendOnSuccess
import com.skydoves.whatif.whatIfNotNull
import kid.app.doctruyen.data.api.ApiService
import kid.app.doctruyen.data.api.request.StoryRequest
import kid.app.doctruyen.data.database.GenreDao
import kid.app.doctruyen.data.model.Category
import kid.app.doctruyen.data.model.Story
import kid.app.doctruyen.utils.constants.AppConstant
import kid.app.doctruyen.utils.network.*
import kotlinx.coroutines.flow.Flow
import timber.log.Timber
import javax.inject.Inject

class StoryDataSource (
//    private val block: suspend (Int) -> Flow<List<Story>>,
//    private val dao: GenreDao
    private val apiClient: ApiClient,
    private val category: Category
) : PagingSource<Int, Story>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Story> {
        val currentPage = params.key ?: AppConstant.STORY_START_PAGE_INDEX
        return try {
            val response = apiClient.fetchStoryByCategory(category = category, currentPage, AppConstant.NETWORK_PAGE_SIZE)
            val stories = response.data?.items ?: emptyList()
            LoadResult.Page(
                    data = stories,
                    prevKey = if (currentPage == AppConstant.STORY_START_PAGE_INDEX) null else (currentPage - 1),
                    nextKey = if (stories.size < AppConstant.NETWORK_PAGE_SIZE) null else (currentPage + 1)
            )
        } catch (e: Exception){
            LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, Story>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }
}