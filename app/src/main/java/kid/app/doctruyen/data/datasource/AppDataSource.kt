package kid.app.doctruyen.data.datasource

import kid.app.doctruyen.data.api.ApiService
import kid.app.doctruyen.data.api.request.StoryByChapterRequest
import kid.app.doctruyen.data.api.request.StoryRequest
import kid.app.doctruyen.utils.network.BaseDataSource
import javax.inject.Inject

class AppDataSource @Inject constructor(
    private val apiService: ApiService
) : BaseDataSource() {

//    suspend fun getCategories() = getResult {
//        apiService.getCategories()
//    }

//    suspend fun getGenres() = getResult {
//        apiService.getGenres()
//    }

    suspend fun getStoryByCategory(link: String, storyRequest: StoryRequest) = apiService.getStoryByCategory(link, storyRequest)
//            getResult {
//        apiService.getStoryByCategory(link, body)
//    }

    suspend fun getStoryDataByChapter(body: StoryByChapterRequest) = getResult { apiService.getStoryByChapter(body) }
}