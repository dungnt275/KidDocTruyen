package kid.app.doctruyen.data.repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.*
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.liveData
import com.skydoves.sandwich.*
import com.skydoves.sandwich.ApiResponse
import com.skydoves.whatif.whatIfNotNull
import kid.app.doctruyen.data.api.ApiService
import kid.app.doctruyen.data.api.request.StoryByChapterRequest
import kid.app.doctruyen.data.database.CategoryDao
import kid.app.doctruyen.data.database.GenreDao
import kid.app.doctruyen.data.model.Category
import kid.app.doctruyen.data.model.Genre
import kid.app.doctruyen.data.datasource.StoryDataSource
import kid.app.doctruyen.data.api.request.StoryRequest
import kid.app.doctruyen.data.datasource.AppDataSource
import kid.app.doctruyen.data.model.Story
import kid.app.doctruyen.utils.constants.AppConstant
import kid.app.doctruyen.utils.network.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import timber.log.Timber
import javax.inject.Inject

//com.example.rickandmorty.data.repository

//https://medium.com/ideas-by-idean/android-adventure-512bbd78b05f
//https://stackoverflow.com/questions/58486364/networkboundresource-with-kotlin-coroutines
//https://stackoverflow.com/questions/57003541/how-to-use-networkboundresource-with-suspend-fun-in-retorfit
//https://github.com/PhilippeBoisney/ArchApp/blob/master/data/repository/src/main/java/io/philippeboisney/repository/utils/NetworkBoundResource.kt
//https://narendrasinhdodiya.medium.com/android-architecture-mvvm-with-coroutines-retrofit-hilt-kotlin-flow-room-48e67ca3b2c8
//https://medium.com/android-news/making-android-jetpacks-networkboundresource-work-in-offline-mode-fd06ef545ec1
//https://www.programmersought.com/article/97145185816/

class AppRepository @Inject constructor(
    private val remoteDataSource: AppDataSource,
    private val apiClient: ApiClient,
    private val categoryDao: CategoryDao,
    private val genreDao: GenreDao)
{

//    fun getCategories() = performGetRequest {
//        remoteDataSource.getCategories()
//    }
//    = performGetOperation(
//        databaseQuery = { categoryDao.getAllCategories() },
//        networkCall = { remoteDataSource.getCategories() },
//        saveCallResult = { it.data?.let { it1 -> categoryDao.insertAll(it1.items) } }
//    )

//    fun getCategories() = performGetRequestWithCache(
//        databaseQuery = {categoryDao.getAllCategories().switchMap { MutableLiveData<BaseNetworkResponse<BaseListObjectResponse<Category>>>().apply {
//            value = BaseNetworkResponse.success(BaseListObjectResponse(items = it) )
//        } } },
//        networkCall = { remoteDataSource.getCategories() },
//        saveCallResult = {  categoryDao.insertAll(it.items)  }
//    )

//    fun getCategories(): LiveData<Resource<BaseListObjectResponse<Category>>>{
//        return object : NetworkBoundResourceFlow<BaseListObjectResponse<Category>, BaseListObjectResponse<Category>>() {
//            override fun onFetchFailed() {
//                Timber.d("fetch failed, should get data from db")
//                super.onFetchFailed()
//            }
//
//            override fun processResponse(response: ApiSuccessResponse<BaseListObjectResponse<Category>>): BaseListObjectResponse<Category> {
//                return super.processResponse(response)
//            }
//
//            override suspend fun saveNetworkResult(item: BaseListObjectResponse<Category>) {
//                Timber.d("the first fetch, so save data to db")
//                categoryDao.insertAll(item.items)
//            }
//
//            override suspend fun fetchFromNetwork(): ApiResponse<BaseListObjectResponse<Category>> {
//                Timber.d("fetching data from server")
//                return apiService.getCategories()
//            }
//
//            override fun shouldFetch(data: BaseListObjectResponse<Category>?): Boolean {
//                if (data != null) {
//                    Timber.d("load data from db FAIL -> fetch data from server")
//                    return data.items == null || data.items.isEmpty()
//                }
//                Timber.d("load data from db OK -> no need to fetch data from server")
//                return false
//            }
//
//            override fun loadFromDb(): Flow<BaseListObjectResponse<Category>> {
//                Timber.d("loading data from db")
//                return flow {
//                    emitAll(categoryDao.getAllCategories().map {
//                        BaseListObjectResponse(items = it)
//                    })
//                }
//            }
//        }.asFlow().asLiveData()
//        return object : NetworkBoundResource<BaseListObjectResponse<Category>, BaseListObjectResponse<Category>>() {
//            override fun onFetchFailed() {
//                super.onFetchFailed()
//            }
//
//            override fun saveCallResult(item: BaseListObjectResponse<Category>) {
//                categoryDao.insertAll(item.items)
//            }
//
//            override fun shouldFetch(data: BaseListObjectResponse<Category>?): Boolean {
//                if (data != null) {
//                    return data.items == null || data.items.isEmpty()
//                }
//                return false
//            }
//
//            override fun loadFromDb(): LiveData<BaseListObjectResponse<Category>> = categoryDao.getAllCategories().switchMap {
//                MutableLiveData<BaseListObjectResponse<Category>>().apply {
//                    value = BaseListObjectResponse(items = it)
//                }
//            }
//
//            override fun createCall(): LiveData<ApiResponse<BaseListObjectResponse<Category>>> = apiService.getCategories()
//        }.asLiveData()
//    }

//    suspend fun getCategories(): Flow<Resource<BaseListObjectResponse<Category>>>{
//        return object : NetworkBoundResourceFlow<BaseListObjectResponse<Category>>(true) {
//            override suspend fun saveNetworkResult(item: BaseListObjectResponse<Category>) {
//                categoryDao.insertAll(item.items)
//            }
//
//            override fun shouldFetch(data: BaseListObjectResponse<Category>?): Boolean {
//                if (data != null) {
//                    return data.items == null || data.items.isEmpty()
//                }
//                return false
//            }
//
//            override fun loadFromDb(): BaseListObjectResponse<Category> {
//                return BaseListObjectResponse(items = categoryDao.getAllCategories())
//            }
//
//            override suspend fun createCall(): ApiResponse<Result<BaseListObjectResponse<Category>>> {
//                return apiService.getCategories()
//            }
//        }.asFlow()
//    }

    @WorkerThread
    fun loadCategories(
        onComplete: () -> Unit,
        onError: (String?) -> Unit
    ) = flow<List<Category>> {
        val categories: List<Category> = categoryDao.getAllCategories()
        if (categories.isEmpty()){
            // request API asynchronously
            Timber.d("call api")
            val response = apiClient.fetchCategoryList()
                // handle the success case when the API request gets a successful response
            response.suspendOnSuccess {
                    data.whatIfNotNull { result ->
                        result.data.whatIfNotNull { list ->
                            Timber.d("insert to db")
                            categoryDao.insertAll(list.items)
                            emit(list.items)
                        }
                    }
                }
                /**
                 * handles error cases when the API request gets an error response.
                 * e.g., internal server error.
                 * maps the [ApiResponse.Failure.Error] to the [BaseErrorResponse] using the mapper.
                 */
                .onError(ErrorResponseMapper) {
                    onError("[Code: $code]: $message")
                }
                // handles exceptional cases when the API request gets an exception response.
                // e.g., network connection error.
                .onException {
                    onError(message())
                }
        } else {
            emit(categories)
        }
    }.onCompletion { onComplete() }.flowOn(Dispatchers.IO)

    @WorkerThread
    fun loadGenres(
        onComplete: () -> Unit,
        onError: (String?) -> Unit
    ) = flow<List<Genre>> {
        val genres: List<Genre> = genreDao.getAllGenres()
        if (genres.isEmpty()){
            // request API asynchronously
            Timber.d("call api")
            val response = apiClient.getGenres()
            // handle the success case when the API request gets a successful response
            response.suspendOnSuccess {
                data.whatIfNotNull { result ->
                    result.data.whatIfNotNull { list ->
                        Timber.d("insert to db")
                        genreDao.insertAll(list.items)
                        emit(list.items)
                    }
                }
            }
                /**
                 * handles error cases when the API request gets an error response.
                 * e.g., internal server error.
                 * maps the [ApiResponse.Failure.Error] to the [BaseErrorResponse] using the mapper.
                 */
                .onError(ErrorResponseMapper) {
                    onError("[Code: $code]: $message")
                }
                // handles exceptional cases when the API request gets an exception response.
                // e.g., network connection error.
                .onException {
                    onError(message())
                }
        } else {
            emit(genres)
        }
    }.onCompletion { onComplete() }.flowOn(Dispatchers.IO)

//    fun getGenres() =  performGetRequestWithCache(
//        databaseQuery = {genreDao.getAllGenres().switchMap { MutableLiveData<BaseNetworkResponse<BaseListObjectResponse<Genre>>>().apply {
//            value = BaseNetworkResponse.success(BaseListObjectResponse(items = it) )
//        } } },
//        networkCall = { remoteDataSource.getGenres() },
//        saveCallResult = {  genreDao.insertAll(it.items)  }
//    )

//    fun getStoryByCategory(link: String, body: StoryRequest) = performGetRequest {
//        remoteDataSource.getStoryByCategory(link, body)
//    }

    @WorkerThread
    fun loadStoryByCategory(category: Category): Flow<PagingData<Story>> = Pager(
        config = PagingConfig(pageSize = AppConstant.NETWORK_PAGE_SIZE),
        pagingSourceFactory = { StoryDataSource(apiClient, category)
//                block = { page -> loadStoryByCategory(category, page, AppConstant.NETWORK_PAGE_SIZE)}
//                dao = genreDao
//            )
        }
    ).flow

//    @WorkerThread
//    fun loadStoryByCategory(category: Category, page: Int, limit: Int,
//        onStart: () -> Unit,
//        onComplete: () -> Unit,
//        onError: (String?) -> Unit) = flow<List<Story>>{
//            val response = apiClient.fetchStoryByCategory(category, page, limit)
//            response.suspendOnSuccess {
//                data.whatIfNotNull { result ->
//                        result.data.whatIfNotNull { list ->
//                            emit(list.items)
//                        }
//                    }
//                }
//                .onError(ErrorResponseMapper) {
//                    onError("[Code: $code]: $message")
//                }
//                .onException {
//                    onError(message())
//                }
//    }.onStart { onStart() }.onCompletion { onComplete() }.flowOn(Dispatchers.IO)


//    private suspend fun getStoryByCategory(category: Category, page: Int, limit: Int): BaseNetworkResponse<BaseListObjectResponse<Story>> = remoteDataSource.getStoryByCategory(link = category.link,
//            storyRequest = StoryRequest(genres = null, page = page, limit = limit))

    fun getStoryDataByChapter(body: StoryByChapterRequest) = performRemoteOperation{
        remoteDataSource.getStoryDataByChapter(body)
    }
}