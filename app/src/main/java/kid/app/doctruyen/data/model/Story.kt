package kid.app.doctruyen.data.model

import android.text.format.DateUtils
import androidx.recyclerview.widget.DiffUtil
import com.google.gson.annotations.SerializedName
import kid.app.doctruyen.utils.constants.AppConstant
import kid.app.doctruyen.utils.datetime.DateTimeConverter
import java.sql.Timestamp
import java.time.Instant

data class Story(
    @SerializedName("author")
    val author: String,
    @SerializedName("source")
    val source: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("genres")
    val genres: List<Int>,
    @SerializedName("ratingValue")
    val ratingValue: Float,
    @SerializedName("bestRating")
    val bestRating: Float,
    @SerializedName("description")
    val description: String,
    @SerializedName("reader")
    val reader: Int,
    @SerializedName("shortName")
    val shortName: String,
    @SerializedName("numbChapter")
    val numbChapter: Int,
    @SerializedName("_id")
    val _id: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("createdAt")
    val createdAt: String,
    @SerializedName("updatedAt")
    val updatedAt: String
) {

    companion object {
        val COMPARATOR = object : DiffUtil.ItemCallback<Story>() {
            override fun areItemsTheSame(oldItem: Story, newItem: Story): Boolean {
                return oldItem.author == newItem.author && oldItem.title == newItem.title
            }

            override fun areContentsTheSame(oldItem: Story, newItem: Story): Boolean {
                return oldItem.description == newItem.description && oldItem.updatedAt == newItem.updatedAt
            }
        }
    }

    val durationFromLastUpdate: String
        get() = DateTimeConverter.convertToReadableDuration(System.currentTimeMillis() - DateTimeConverter.fromDateToMilliseconds(updatedAt, AppConstant.DATE_TIME.DATE_FORMAT2))

    var genresInString = emptyList<String>()
}
