package kid.app.doctruyen.data.api.request

data class StoryByChapterRequest(
        val truyenId: String,
        val chapterId: String
)
