package kid.app.doctruyen.data.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "categories")
@Parcelize
data class Category(
    @SerializedName("_id")
    @PrimaryKey
    val _id: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("link")
    val link: String,
    @SerializedName("createAt")
    val createAt: String?,
    @SerializedName("updateAt")
    val updateAt: String?,
    @SerializedName("active")
    val active: Boolean?,
    @SerializedName("__v")
    val __v: Int = 0
): Parcelable
