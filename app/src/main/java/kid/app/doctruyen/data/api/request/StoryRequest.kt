package kid.app.doctruyen.data.api.request

import com.google.gson.annotations.SerializedName

data class StoryRequest(
    val genres: Int?,
    val page: Int,
    val limit: Int
)
