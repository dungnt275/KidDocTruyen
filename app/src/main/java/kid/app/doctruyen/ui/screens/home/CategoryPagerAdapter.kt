package kid.app.doctruyen.ui.screens.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import kid.app.doctruyen.data.model.Category
import kid.app.doctruyen.utils.constants.AppConstant

class CategoryPagerAdapter(fragment: Fragment, private val tabs: List<Category>): FragmentStateAdapter(fragment) {

    override fun getItemCount() = tabs.size

    override fun createFragment(position: Int): Fragment {
        val page = ListStoryFragment()
        return page.apply {
            arguments = Bundle().apply {
                putParcelable(AppConstant.ARGS.KEY_CATEGORY, tabs[position])
            }
        }
    }

    fun getCategoryList() = tabs
}