package kid.app.doctruyen.ui.screens.home

import androidx.databinding.Bindable
import androidx.hilt.Assisted
import kid.app.doctruyen.data.repository.AppRepository
import kid.app.doctruyen.ui.base.BaseViewModel
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.skydoves.bindables.bindingProperty
import kid.app.doctruyen.data.database.CategoryDao
import kid.app.doctruyen.data.model.Category
import kid.app.doctruyen.ui.base.LiveCoroutinesViewModel
import kid.app.doctruyen.utils.network.BaseListObjectResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.launch
import timber.log.Timber

class HomeViewModel @ViewModelInject constructor(
    private val repository: AppRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
): BaseViewModel() {

    val categoriesLiveData = repository.loadCategories(
        onComplete = { Timber.d("complete") },
        onError = { Timber.d("error") }
    ).asLiveData(viewModelScope.coroutineContext + Dispatchers.IO)

//    @get:Bindable
//    var isLoading: Boolean by bindingProperty(true)
//        private set
//
//    @get:Bindable
//    var errorToast: String? by bindingProperty(null)
//        private set

//    init {
//        categoriesLiveData = repository.loadCategories(
//                onComplete = { isLoading = false },
//                onError = { errorToast = it }
//            ).asLiveDateOnViewModelScope()
//        Timber.d("categories size: ${categoriesLiveData.value?.size}")
//    }
//    val genres = repository.getGenres()
}