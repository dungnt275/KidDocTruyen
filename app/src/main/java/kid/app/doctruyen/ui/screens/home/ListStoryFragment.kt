package kid.app.doctruyen.ui.screens.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.AndroidEntryPoint
import kid.app.doctruyen.R
import kid.app.doctruyen.data.model.Category
import kid.app.doctruyen.databinding.FragmentListStoryBinding
import kid.app.doctruyen.ui.adapter.NetworkStateAdapter
import kid.app.doctruyen.ui.custom.StoryItemDecoration
import kid.app.doctruyen.utils.autoCleared
import kid.app.doctruyen.utils.constants.AppConstant
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.filter

//https://github.com/zivkesten/Paging-with-Flow-sample-app
@AndroidEntryPoint
class ListStoryFragment : Fragment() {
    private val viewModel: ListStoryViewModel by viewModels()
    private var binding: FragmentListStoryBinding by autoCleared()
    private var storyAdapter by autoCleared<StoryAdapter>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.apply {
            getParcelable<Category>(AppConstant.ARGS.KEY_CATEGORY)?.let {
//                viewModel.showStoriesByCategory(it)
            }
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = FragmentListStoryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupStoryList()
        setupViewModel()
        setupListener()
    }

    private fun setupViewModel(){
        viewModel.stories.observe(viewLifecycleOwner) {
            storyAdapter.submitData(viewLifecycleOwner.lifecycle, it)
        }
    }

    private fun setupStoryList(){
        binding.storySwipeRefresh.setOnRefreshListener {
            storyAdapter.refresh()
        }
        storyAdapter = StoryAdapter { story ->
            //navigate to detail
            findNavController().navigate(HomeFragmentDirections.actionNavigationHomeToFragmentReadingStory(story._id))
        }
        binding.storyRecycler.adapter = storyAdapter.withLoadStateHeaderAndFooter(
                header = NetworkStateAdapter { storyAdapter.retry() },
                footer = NetworkStateAdapter { storyAdapter.retry() }
        )
        val divider =
                ContextCompat.getDrawable(context ?: return, R.drawable.shape_story_divider) ?: return
        val horizontalSpacing = resources.getDimensionPixelOffset(R.dimen.dp_16)
        val itemDecoration = StoryItemDecoration(horizontalSpacing, divider)
        binding.apply {
            storyRecycler.addItemDecoration(itemDecoration)
//            storyRecycler.addOnItemTouchListener(object : RecyclerView.OnItemTouchListener {
//                override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
//                    when (e.action) {
//                        MotionEvent.ACTION_DOWN -> {
//                            storyRecycler.parent?.requestDisallowInterceptTouchEvent(true)
//                        }
//                    }
//                    return false
//                }
//
//                override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {}
//
//                override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {}
//            })
        }
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            storyAdapter.loadStateFlow.collectLatest {
                binding.storySwipeRefresh.isRefreshing = it.refresh is LoadState.Loading
                binding.retryButton.isVisible = it.refresh is LoadState.Error
                binding.progress.isVisible = it.refresh is LoadState.Loading
                binding.storyRecycler.isVisible =
                        it.refresh !is LoadState.Error && it.refresh is LoadState.NotLoading
            }
        }
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            storyAdapter.loadStateFlow
                    .distinctUntilChangedBy { it.refresh }
                    .filter { it.refresh is LoadState.NotLoading }
                    .collectLatest { binding.storyRecycler.scrollToPosition(0) }
        }
    }

    private fun setupListener(){
        binding.retryButton.setOnClickListener {
            viewModel.retryGetStories()
        }
    }
}