package kid.app.doctruyen.ui.screens.home

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import androidx.paging.cachedIn
import kid.app.doctruyen.data.database.CategoryDao
import kid.app.doctruyen.data.model.Category
import kid.app.doctruyen.data.repository.AppRepository
import kid.app.doctruyen.ui.base.BaseViewModel
import kid.app.doctruyen.utils.constants.AppConstant
import kotlinx.coroutines.Dispatchers

class ListStoryViewModel @ViewModelInject constructor(
        private val repository: AppRepository,
        private val categoryDao: CategoryDao,
        @Assisted private val savedStateHandle: SavedStateHandle
): BaseViewModel() {

    private val _savedLink = savedStateHandle.getLiveData<Category>(AppConstant.ARGS.KEY_CATEGORY)
    val stories = _savedLink.switchMap {
        repository.loadStoryByCategory(it).asLiveData(viewModelScope.coroutineContext + Dispatchers.IO).cachedIn(viewModelScope)
    }

    fun showStoriesByCategory(category: Category){
        if (savedStateHandle.get<Category>(AppConstant.ARGS.KEY_CATEGORY) == category) return
        savedStateHandle.set(AppConstant.ARGS.KEY_CATEGORY, category)
    }

    fun retryGetStories() {
//        val savedGenre = savedStateHandle.get<String>(AppConstant.KEY_CATEGORY) ?: return
//        savedStateHandle.set(AppConstant.KEY_CATEGORY, savedGenre)
    }

    fun <T, K, R> LiveData<T>.combineWith(
            liveData: LiveData<K>,
            block: (T?, K?) -> R
    ): LiveData<R> {
        val result = MediatorLiveData<R>()
        result.addSource(this) {
            result.value = block(this.value, liveData.value)
        }
        result.addSource(liveData) {
            result.value = block(this.value, liveData.value)
        }
        return result
    }

}