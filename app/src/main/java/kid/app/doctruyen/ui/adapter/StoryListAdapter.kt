package kid.app.doctruyen.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import kid.app.doctruyen.R
import kid.app.doctruyen.data.model.Story
import kid.app.doctruyen.databinding.ItemStoryViewHolderBinding
import kid.app.doctruyen.ui.common.DataBoundListAdapter
import kid.app.doctruyen.ui.common.DataBoundViewHolder
import kid.app.doctruyen.utils.AppExecutors

class StoryListAdapter(
    private val dataBindingComponent: DataBindingComponent,
    appExecutors: AppExecutors,
    private val storyClickCallback:((Story) -> Unit)?
) : DataBoundListAdapter<Story, ItemStoryViewHolderBinding>(
    appExecutors = appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<Story>(){
        override fun areItemsTheSame(oldItem: Story, newItem: Story): Boolean {
            return oldItem.author == newItem.author && oldItem.title == newItem.title
        }

        override fun areContentsTheSame(oldItem: Story, newItem: Story): Boolean {
            return oldItem.description == newItem.description && oldItem.updatedAt == newItem.updatedAt
        }
    }
){

    override fun bind(binding: ItemStoryViewHolderBinding, item: Story) {
        binding.story = item
    }

    override fun createBinding(parent: ViewGroup): ItemStoryViewHolderBinding {
        val binding = DataBindingUtil.inflate<ItemStoryViewHolderBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_story_view_holder,
            parent,
            false,
            dataBindingComponent)

        binding.root.setOnClickListener {
            binding.story?.let {
                storyClickCallback?.invoke(it)
            }
        }

        return binding
    }
}