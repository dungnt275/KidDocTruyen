package kid.app.doctruyen.ui.screens.reading

import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.bidv.common.utils.datastore.SharePreferencesUtils
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.slider.Slider
import dagger.hilt.android.AndroidEntryPoint
import kid.app.doctruyen.R
import kid.app.doctruyen.databinding.ContentStoryBinding
import kid.app.doctruyen.databinding.FragmentReadingStoryBinding
import kid.app.doctruyen.databinding.ReadingBottomSheetBinding
import kid.app.doctruyen.utils.autoCleared
import kid.app.doctruyen.utils.network.Status
import kotlinx.android.synthetic.main.reading_bottom_sheet.*
import timber.log.Timber

@AndroidEntryPoint
class ReadingStoryFragment: Fragment(), View.OnClickListener {
    private var viewBinding: FragmentReadingStoryBinding by autoCleared()
    private val args: ReadingStoryFragmentArgs by navArgs()
    protected val viewModel: ReadingStoryViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewBinding = FragmentReadingStoryBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (args != null){
            viewModel.storyId.value = args.truyenId
            initUI()
        }
    }

    private fun initUI(){
        viewModel.dataStory.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    viewBinding.textStoryData.text = it.data?.data?.data ?: "Co loi xay ra"
                    val savedTruyenId = SharePreferencesUtils.getString("truyenId", requireContext())
                    Timber.d("get saved truyenId ${savedTruyenId}")
                    Timber.d("args.truyenId ${args.truyenId}")
                    if (savedTruyenId == args.truyenId) {
                        Timber.d("get saved positionY ${SharePreferencesUtils.getInt("positionY", requireContext())}")
                        viewBinding.scr.post { viewBinding.scr.smoothScrollTo(0, SharePreferencesUtils.getInt("positionY", requireContext())) }
                    }
                }
                Status.ERROR -> {
                    Timber.d("error load data story")
                }
                Status.LOADING -> {
                    Timber.d("Loading...")
                }
            }
        })

        viewBinding.fontSizeSlider.addOnChangeListener(object : Slider.OnChangeListener {
            override fun onValueChange(slider: Slider, value: Float, fromUser: Boolean) {
                viewBinding.textStoryData.setTextSize(TypedValue.COMPLEX_UNIT_PX,value)
            }
        })
    }

//    override fun onSaveInstanceState(outState: Bundle) {
//        super.onSaveInstanceState(outState)
//        Timber.d("call onSaveInstanceState")
//        Timber.d("save positionY= ${viewBinding.scr.scrollY}")
//        outState.putInt("positionY", viewBinding.scr.scrollY)
//    }
//
//    override fun onViewStateRestored(savedInstanceState: Bundle?) {
//        super.onViewStateRestored(savedInstanceState)
//        Timber.d("call onViewStateRestored")
//        Timber.d("restore positionY= ${savedInstanceState?.getInt("positionY") ?: 0}")
//        val posY = savedInstanceState?.getInt("positionY") ?: 0
//        viewBinding.scr.scrollY = posY
//    }

    override fun onClick(v: View?) {

        when (v!!.id) {
//            R.id.btnFeedback -> {
//                val bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet)
//                if (bottomSheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
//                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
//
//                } else {
//                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
//                }
//            }

//            R.id.imageClose -> {
//                val bottomSheetBehavior = BottomSheetBehavior.from(viewBinding.myBottomSheet)
//                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
//            }
        }
    }

    override fun onStop() {
        super.onStop()
        Timber.d("call onStop")
        SharePreferencesUtils.saveString("truyenId", viewModel.storyId?.value ?: "", requireContext())
        Timber.d("save truyenId ${viewModel.storyId?.value ?: ""}")
        SharePreferencesUtils.saveInt("positionY", viewBinding.scr.scrollY, requireContext())
        Timber.d("save positionY ${viewBinding.scr.scrollY}")
    }
}