package kid.app.doctruyen.ui.screens.genre

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import kid.app.doctruyen.R
import kid.app.doctruyen.data.model.Genre
import kid.app.doctruyen.databinding.ItemGenreViewHolderBinding
import kid.app.doctruyen.ui.common.DataBoundListAdapter
import kid.app.doctruyen.utils.AppExecutors

class GenresAdapter(
    appExecutors: AppExecutors,
    private val clickCallBack: ((Genre) -> Unit)?
): DataBoundListAdapter<Genre, ItemGenreViewHolderBinding>(
    appExecutors = appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<Genre>(){
        override fun areItemsTheSame(oldItem: Genre, newItem: Genre): Boolean {
            return oldItem.genreId == newItem.genreId && oldItem.name == newItem.name
        }

        override fun areContentsTheSame(oldItem: Genre, newItem: Genre): Boolean {
            return oldItem.genreId == newItem.genreId && oldItem.total == newItem.total
        }
    }
) {

    override fun createBinding(parent: ViewGroup): ItemGenreViewHolderBinding {
        val binding = DataBindingUtil.inflate<ItemGenreViewHolderBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_genre_view_holder,
            parent,
            false
        )
        binding.root.setOnClickListener {
            binding.genre?.let {
                clickCallBack?.invoke(it)
            }
        }
        return binding
    }

    override fun bind(binding: ItemGenreViewHolderBinding, item: Genre) {
        binding.genre = item
    }
}