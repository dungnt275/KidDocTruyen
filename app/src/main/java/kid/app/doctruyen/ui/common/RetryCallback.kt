package kid.app.doctruyen.ui.common

/**
 * Generic interface for retry buttons.
 */
interface RetryCallback {
    fun retry()
}
