package kid.app.doctruyen.ui.screens.main

import android.os.Bundle
import android.view.View
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import dagger.hilt.android.AndroidEntryPoint
import kid.app.doctruyen.R
import kid.app.doctruyen.databinding.ActivityMainBinding

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var viewBinding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)

        setupNavController()

    }

    private fun setupNavController(){
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.nav_host_fragment)
        val navGraph =navController.navInflater.inflate(R.navigation.main_navigation)
        navController.graph = navGraph
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
//        val appBarConfiguration = AppBarConfiguration(setOf(
//                R.id.navigation_explore, R.id.navigation_category, R.id.navigation_search, R.id.navigation_history, R.id.navigation_user))
//        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        navController.addOnDestinationChangedListener(object : NavController.OnDestinationChangedListener {
            override fun onDestinationChanged(controller: NavController, destination: NavDestination, arguments: Bundle?) {
                when (destination.id){
                    R.id.navigation_home -> viewBinding.navView.visibility = View.VISIBLE
                    else -> viewBinding.navView.visibility = View.GONE
                }
            }
        })
    }

    private fun hideBottomNavigation() {
        // bottom_navigation is BottomNavigationView
        with(viewBinding.navView) {
            if (visibility == View.VISIBLE && alpha == 1f) {
                animate()
                        .alpha(0f)
                        .withEndAction { visibility = View.GONE }
                        .duration = 1//EXIT_DURATION
            }
        }
    }

    private fun showBottomNavigation() {
        // bottom_navigation is BottomNavigationView
        with(viewBinding.navView) {
            visibility = View.VISIBLE
            animate()
                    .alpha(1f)
                    .duration = 1//ENTER_DURATION
        }
    }
}