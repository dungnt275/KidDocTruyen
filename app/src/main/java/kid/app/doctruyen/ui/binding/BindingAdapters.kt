package kid.app.doctruyen.ui.binding

import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.transition.TransitionManager
import android.view.View
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.databinding.BindingAdapter
import androidx.fragment.app.findFragment
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.bidv.common.utils.device.DimentionUtil
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestListener
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.AppBarLayout.OnOffsetChangedListener
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.google.android.material.transition.platform.MaterialArcMotion
import com.google.android.material.transition.platform.MaterialContainerTransform
import com.skydoves.whatif.whatIfNotNull
import com.skydoves.whatif.whatIfNotNullAs
import com.skydoves.whatif.whatIfNotNullOrEmpty
import kid.app.doctruyen.R
import kid.app.doctruyen.data.model.Category
import kid.app.doctruyen.data.model.Genre
import kid.app.doctruyen.ui.extension.gone
import kid.app.doctruyen.ui.extension.visible
import kid.app.doctruyen.ui.screens.home.CategoryPagerAdapter
import kotlin.math.abs

@BindingAdapter("loadImage")
fun bindLoadImage(view: AppCompatImageView, url: String?) {
    Glide.with(view.context)
            .load(url)
            .into(view)
}

@BindingAdapter("pagerAdapter")
fun bindPagerAdapter(view: ViewPager2, adapter: FragmentStateAdapter) {
    view.adapter = adapter
    view.offscreenPageLimit = 3
}

@BindingAdapter("bindFab")
fun bindAppBarLayoutWithFab(appBarLayout: AppBarLayout, fab: FloatingActionButton) {
    appBarLayout.addOnOffsetChangedListener(
            OnOffsetChangedListener { appBarLayout1: AppBarLayout, verticalOffset: Int ->
              val verticalOffsetPercentage = abs(
                      verticalOffset
              ).toFloat() / appBarLayout1.totalScrollRange.toFloat()
              if (verticalOffsetPercentage > 0.4f && fab.isOrWillBeShown) {
                fab.hide()
              } else if (verticalOffsetPercentage <= 0.4f && fab.isOrWillBeHidden && fab.tag != View.GONE) {
                fab.show()
              }
            }
    )
}

@BindingAdapter("transformFab", "transformContainer")
fun bindTransformFab(view: View, fab: FloatingActionButton, container: CoordinatorLayout) {
    fab.setOnClickListener {
        // Begin the transition by changing properties on the start and end views or
        // removing/adding them from the hierarchy.
        fab.tag = View.GONE
        TransitionManager.beginDelayedTransition(container, getTransform(fab, view))
        fab.gone(true)
        view.visible()
    }

    view.setOnClickListener {
        fab.tag = View.VISIBLE
        TransitionManager.beginDelayedTransition(container, getTransform(view, fab))
        fab.visible()
        view.gone(true)
    }
}

private fun getTransform(mStartView: View, mEndView: View): MaterialContainerTransform {
    return MaterialContainerTransform().apply {
        startView = mStartView
        endView = mEndView
        addTarget(mEndView)
        pathMotion = MaterialArcMotion()
        duration = 550
        scrimColor = Color.TRANSPARENT
    }
}

@BindingAdapter("items")
fun setItems(view: ChipGroup, genres: List<String>?) {
    if (genres == null // Since we are using liveData to observe data, any changes in that table(favorites)
            // will trigger the observer and hence rebinding data, which can lead to duplicates.
            || view.childCount > 0
    ) return

    // dynamically create & add genre chips
    val context = view.context
    for (genre in genres) {
        val chip = Chip(context)
        chip.text = genre
        chip.chipStrokeWidth = DimentionUtil.dpToPx(1f, context)
        chip.chipStrokeColor = ColorStateList.valueOf(
                context.resources.getColor(R.color.md_blue_grey_200)
        )
        chip.setChipBackgroundColorResource(R.color.transparent)
        view.addView(chip)
    }
}

@BindingAdapter("gone")
fun bindGone(view: View, shouldBeGone: Boolean?) {
    if (shouldBeGone == true) {
        view.gone(true)
    } else {
        view.gone(false)
    }
}

@BindingAdapter(value = ["imageUrl", "imageRequestListener"], requireAll = false)
fun bindImage(imageView: ImageView, url: String?, listener: RequestListener<Drawable?>?) {
    Glide.with(imageView.context).load(url).listener(listener).into(imageView)
}

//@BindingAdapter("pager")
//fun setPager(tabLayout: TabLayout, viewpager: ViewPager2) {
//    viewpager.adapter.whatIfNotNullAs<CategoryPagerAdapter> {
//        TabLayoutMediator(tabLayout, viewpager) { tab, position ->
//            tab.text = it.getCategoryList()[position].name
//        }.attach()
//    }
//}
//
//@BindingAdapter("adapterCategoryList")
//fun bindAdapterCategoryList(view: ViewPager2, listCategory: List<Category>?) {
//    listCategory.whatIfNotNullOrEmpty {
//        view.adapter = CategoryPagerAdapter(view.findFragment(), it)
//    }
//    view.adapter.whatIfNotNullAs<CategoryPagerAdapter> { adapter ->
//        view.offscreenPageLimit = if (adapter.itemCount > 1)  adapter.itemCount - 1 else 1
//    }
//}
