package kid.app.doctruyen.ui.screens.genre

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import kid.app.doctruyen.data.database.CategoryDao
import kid.app.doctruyen.data.database.GenreDao
import kid.app.doctruyen.data.repository.AppRepository
import kid.app.doctruyen.ui.base.BaseViewModel
import kotlinx.coroutines.Dispatchers

class GenreViewModel @ViewModelInject constructor(
    private val repository: AppRepository,
    private val genreDao: GenreDao,
    @Assisted private val savedStateHandle: SavedStateHandle
) : BaseViewModel() {

    val genres = repository.loadGenres(
        onComplete = {},
        onError = {}
    ).asLiveData(viewModelScope.coroutineContext + Dispatchers.IO)
}