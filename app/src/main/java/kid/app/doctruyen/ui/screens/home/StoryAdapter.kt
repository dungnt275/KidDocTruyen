package kid.app.doctruyen.ui.screens.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import kid.app.doctruyen.data.model.Story
import kid.app.doctruyen.databinding.ItemStoryViewHolderBinding
import kid.app.doctruyen.extensions.setSafeClick
import kid.app.doctruyen.ui.base.BaseViewHolder

class StoryAdapter(private val onClick: (Story) -> Unit) :
    PagingDataAdapter<Story, BaseViewHolder<ItemStoryViewHolderBinding>>(diffCallback = Story.COMPARATOR){

    override fun onBindViewHolder(
        holder: BaseViewHolder<ItemStoryViewHolderBinding>,
        position: Int
    ) {
        val item = getItem(position) ?: return
        holder.binding.apply {
            story = item
            itemStoryRoot.setSafeClick(View.OnClickListener { onClick.invoke(item) })
            executePendingBindings()
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<ItemStoryViewHolderBinding> {
        val binding = ItemStoryViewHolderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return BaseViewHolder(binding)
    }
}