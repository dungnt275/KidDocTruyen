package kid.app.doctruyen.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import kid.app.doctruyen.R
import kid.app.doctruyen.databinding.ItemNetworkStateBinding
import kid.app.doctruyen.extensions.setSafeClick
import kid.app.doctruyen.ui.base.BaseViewHolder

/**
 * The adapter class handle logic of network footer items of a Paging RecyclerView
 * Maybe action: retry, refresh
 * Maybe state: loading, done...
 */
class NetworkStateAdapter(private val retry: (() -> Unit)) :
    LoadStateAdapter<BaseViewHolder<ItemNetworkStateBinding>>() {

    override fun onBindViewHolder(
        holder: BaseViewHolder<ItemNetworkStateBinding>, loadState: LoadState
    ) {
        holder.binding.apply {
            isLoading = loadState == LoadState.Loading
            if (loadState is LoadState.Error) {
                message = holder.itemView.context.getString(R.string.no_connection_error)
            }
            executePendingBindings()
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup, loadState: LoadState
    ): BaseViewHolder<ItemNetworkStateBinding> {
        val binding =
            ItemNetworkStateBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        binding.retryButton.setSafeClick(View.OnClickListener { retry.invoke() })
        return BaseViewHolder(binding)
    }

}
