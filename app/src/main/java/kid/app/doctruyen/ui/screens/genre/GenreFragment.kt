package kid.app.doctruyen.ui.screens.genre

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import dagger.hilt.android.AndroidEntryPoint
import kid.app.doctruyen.databinding.FragmentGenreBinding
import kid.app.doctruyen.utils.AppExecutors
import kid.app.doctruyen.utils.autoCleared
import kid.app.doctruyen.utils.network.Status
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class GenreFragment : Fragment() {

    private val genreViewModel: GenreViewModel by viewModels()
    private var binding by autoCleared<FragmentGenreBinding>()
    private var adapter by autoCleared<GenresAdapter>()
    @Inject
    lateinit var appExecutors: AppExecutors

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = FragmentGenreBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupListGenres()
    }

    private fun setupListGenres(){
        val rvAdapter = GenresAdapter(
            appExecutors = appExecutors
        ) {
            genre -> Timber.d("click ${genre.name}")
        }
        binding.rvListGenre.adapter = rvAdapter
        this.adapter = rvAdapter
        initGenresList()
    }

    private fun initGenresList(){
        genreViewModel.genres.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
        })
    }
}