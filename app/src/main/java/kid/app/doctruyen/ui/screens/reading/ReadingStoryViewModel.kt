package kid.app.doctruyen.ui.screens.reading

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import kid.app.doctruyen.data.api.request.StoryByChapterRequest
import kid.app.doctruyen.data.repository.AppRepository
import kid.app.doctruyen.ui.base.BaseViewModel

class ReadingStoryViewModel @ViewModelInject constructor( private val repository: AppRepository) : BaseViewModel() {

//    var scrollPos: Int? = null
    val storyId = MutableLiveData<String>()

    val dataStory = Transformations.switchMap(storyId) {
        repository.getStoryDataByChapter(StoryByChapterRequest(truyenId = it, chapterId = "chuong-1"))
    }
}