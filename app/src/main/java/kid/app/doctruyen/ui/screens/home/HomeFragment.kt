package kid.app.doctruyen.ui.screens.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint
import kid.app.doctruyen.data.model.Category
import kid.app.doctruyen.databinding.FragmentHomeBinding
import kid.app.doctruyen.utils.autoCleared
import kid.app.doctruyen.utils.network.Status

@AndroidEntryPoint
class HomeFragment : Fragment() {
    //https://github.com/fs-sournary/Android-architecture-component.git
    private val viewModel: HomeViewModel by viewModels()
    private var binding: FragmentHomeBinding by autoCleared()
    private lateinit var categoryPagerAdapter: CategoryPagerAdapter

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViewModel()
        setupListener()
    }

    private fun setupTabs(tabs: List<Category>){
        categoryPagerAdapter = CategoryPagerAdapter(this, tabs)
        val limit = if (categoryPagerAdapter.itemCount > 1)  categoryPagerAdapter.itemCount - 1 else 1
        binding.apply {
            listStoryPager.adapter = categoryPagerAdapter
            listStoryPager.offscreenPageLimit = limit
            TabLayoutMediator(tabStoryCategory, listStoryPager) { tab, position ->
                tab.text = tabs[position].name
//                tab.tag = tabs[position].link
            }.attach()
        }
    }

    private fun setupViewModel(){
        viewModel.categoriesLiveData.observe(viewLifecycleOwner, Observer
            {setupTabs(it)
//            when (it.status){
//                Status.SUCCESS -> {
//                    if (it.isSuccess && it.data?.items?.size!! > 0) setupTabs((it.data.items))
//                }
//                Status.ERROR -> {
//
//                }
//                Status.LOADING -> {
//
//                }
//            }
        })
//        viewModel.genres.observe(viewLifecycleOwner, Observer {  })
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }
    private fun setupListener(){
//        binding.apply {
//            tabStoryCategory.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
//
//                override fun onTabSelected(tab: TabLayout.Tab?) {
//                    viewModel.showStoriesByCategory(tab?.tag as String)
//                }
//
//                override fun onTabUnselected(tab: TabLayout.Tab?) {
//                }
//
//                override fun onTabReselected(tab: TabLayout.Tab?) {
//                }
//            })
//        }
//        binding.retryButton.setOnClickListener {
//            viewModel.retryGetStories()
//        }
    }

}
